import { DatabaseApi } from "./database-service";
import { Category, Concept, Keyword, TextEntity } from "../typings";
import * as Report from "../entities/report";

export abstract class ReportApi extends DatabaseApi {
  abstract saveEntity(
    articleId: number,
    reportArticle: Report.Article
  ): (entity: TextEntity) => void;

  abstract saveCategory(
    articleId: number,
    reportArticle: Report.Article
  ): (category: Category) => void;

  abstract saveKeyword(
    articleId: number,
    reportArticle: Report.Article
  ): (keyword: Keyword) => void;

  abstract saveConcept(
    articleId: number,
    reportArticle: Report.Article
  ): (concept: Concept) => void;

  // abstract getCategoryByLabel(label: string): Promise<Report.Category>
}
