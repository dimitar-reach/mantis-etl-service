import { Inject, Provides, Singleton } from 'typescript-ioc';

import { MantisApi } from './mantis.api';
import { Article, Rating, Ruleset, Category, User } from '../entities/mantis'
import { DatabaseService } from './database-service'
import { ServiceConfig } from '../config/service.config'
import { Findings } from '../typings'


@Singleton
@Provides(MantisApi)
export class MantisService extends DatabaseService implements MantisApi {

  constructor(@Inject config: ServiceConfig) {
    super('Mantis', config.mantis)
  }

  async getArticleById(id: number): Promise<Article> {
    const connection = await this.getConnection()

    try {
      return await connection
        .getRepository(Article)
        .findOne(id)

    } catch (e) {
      this.logger.error(e, e.query)
    }
  }

  async getArticleFindings(id: number): Promise<Findings> {
    const connection = await this.getConnection()

    try {
      const article: Article = await connection
        .getRepository(Article)
        .findOne(id)

      return article.findings

    } catch (e) {
      this.logger.error(e, e.query)
    }
  }

  async getRatingsByArticleId(articleId: number): Promise<Rating[]> {
    const connection = await this.getConnection()

    try {
      return await connection
        .getRepository(Rating)
        .createQueryBuilder('rating')
        .where('rating.articles_id = :articles_id', { articles_id: articleId })
        .getMany()

    } catch (e) {
      this.logger.error(e, e.query)
    }
  }
  
  async getCategoriesByArticleId(articleId: number): Promise<Category[]> {
    const connection = await this.getConnection()

    try {
      return await connection
        .getRepository(Category)
        .createQueryBuilder('category')
        .where('category.articles_id = :articles_id', { articles_id: articleId })
        .getMany();

    } catch (e) {
      this.logger.error(e, e.query)
    }
  }

  async getUserNameById(id: string): Promise<string> {
    const connection = await this.getConnection()

    try {
      const user = await connection
        .getRepository(User)
        .createQueryBuilder('user')
        .where('id = :id', { id: id })
        .getOne();
      return (user || {}).username;
    } catch (e) {
      this.logger.error(e, e.query)
    }
  } 

  async getAllArticleIDs(): Promise<number[]> {
    const connection = await this.getConnection()

    try {
      const articles = await connection
        .getRepository(Article)
        .createQueryBuilder('article')
        .select('article.id')
        .getMany()

      return articles.map(article => article.id)

    } catch (e) {
      this.logger.error(e, e.query)
    }
  }

  async getAllRulesetIDs(): Promise<number[]> {
    const connection = await this.getConnection()

    try {
      const rulesets = await connection
        .getRepository(Ruleset)
        .createQueryBuilder('ruleset')
        .select('ruleset.id')
        .getMany()

      return rulesets.map(ruleset => ruleset.id)

    } catch (e) {
      this.logger.error(e, e.query)
    }
  }

  // async getUnsyncedArticles()

}
