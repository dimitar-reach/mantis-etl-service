import { Inject, Provides, Singleton } from "typescript-ioc";
import { Connection, InsertResult } from 'typeorm' ;
import * as Mantis from "../entities/mantis";
import * as Report from "../entities/report";
import { ServiceConfig } from "../config/service.config";
import { TransformApi } from "./transform.api";
import { LoggerApi } from "../logger";
import { MantisApi } from "./mantis.api";
import { ReportApi } from "./report.api";
import { Category, Concept, Keyword, TextEntity } from "../typings";

@Singleton
@Provides(TransformApi)
export class TransformService implements TransformApi {
  @Inject mantis: MantisApi;
  @Inject report: ReportApi;
  @Inject config: ServiceConfig;
  logger: LoggerApi;
  processedArticles: number;
  activeTransformations: number;

  constructor(
    @Inject
      logger: LoggerApi
  ) {
    this.logger = logger.child("TransformService");
    this.activeTransformations = 0;
    this.processedArticles = 0;
  }

  private incrementActiveTransformations() {
    ++this.activeTransformations;
  }

  private decrementActiveTransformations() {
    --this.activeTransformations;
  }

  private async transformRatings(
    articleId: number,
    reportArticle: Report.Article
  ) {
    const reportConnection = await this.report.getConnection();
    const mantisConnection = await this.mantis.getConnection();

    const mantisRatings: Mantis.Rating[] = await this.mantis.getRatingsByArticleId(
      articleId
    );

    if (typeof mantisRatings === "undefined") {
      this.logger.error(
        `Failed at fetching ratings for article ID ${articleId} -- skipping.`
      );
      return;
    }

    await Promise.all(
      mantisRatings.map(async (rating) => {
        try {
          const article = reportConnection
            .getRepository(Report.Article)
            .merge(new Report.Article(), { id: articleId });

          const segment = await mantisConnection.
          getRepository(Mantis.ContextualSegment)
          .find({ where: { id: rating.contextual_segments_id }, relations: ['rulesets']});

          const username = await this.mantis.getUserNameById(rating.review_user_id);

          const insertRatingsPromise = segment[0].rulesets.map((ruleset) => {
            const reportArticleRatings = reportConnection
            .getRepository(Report.ArticleRatings)
            .merge(new Report.ArticleRatings(), reportArticle, rating, {
              article_id: articleId, customer: segment[0].name,
              rulesets_version: ruleset.version,
              review_user_id: username
            });


          // since a contextual_segment_id has 
          return reportConnection.createQueryBuilder()
          .insert()
          .into(Report.ArticleRatings)
          .onConflict('("customer", "rulesets_version", "article_id") DO UPDATE SET calculated_rating = :calculated_rating, review_date = :review_date')
          .setParameter('calculated_rating', rating.calculated_rating)
          .setParameter('review_date', rating.review_date)
          .values([reportArticleRatings]).execute()
          });

          await Promise.all(insertRatingsPromise);
        } catch (e) {
          this.logger.error(e, e.query);
        }
      })
    );
  }

  private async transformMantisArticleCategories(articleId: number, reportArticle: Report.Article) {
    const reportConnection = await this.report.getConnection();
    const mantisConnection = await this.mantis.getConnection();

    const mantisCategories: Mantis.Category[] = await this.mantis.getCategoriesByArticleId(articleId);

    if (typeof mantisCategories === "undefined") {
      this.logger.error(
        `Failed at fetching ratings for article ID ${articleId} -- skipping.`
      );
      return;
    }

    await Promise.all(
      mantisCategories.map(async (category) => {
        try {
          const article = reportConnection
            .getRepository(Report.Article)
            .merge(new Report.Article(), { id: articleId });

          const segment = await mantisConnection.
          getRepository(Mantis.ContextualSegment)
          .find({ where: { id: category.contextual_segments_id }, relations: ['rulesets']});

          this.logger.info(`Segment found: `, segment);
          const username = await this.mantis.getUserNameById(category.review_user_id);

          const insertRatingsPromise = segment[0].rulesets.map((ruleset) => {
            const reportArticleContextualCategory = reportConnection
            .getRepository(Report.ArticleContextualCategories)
            .merge(new Report.ArticleContextualCategories(), reportArticle, category, {
              article_id: articleId, customer: segment[0].name,
              rulesets_version: ruleset.version,
              review_user_id: username,
              review_date: category.creation_date
            });
            this.logger.info(`Insert promise: `, reportArticleContextualCategory);
            return reportConnection.createQueryBuilder()
              .insert()
              .into(Report.ArticleContextualCategories)
              .onConflict('("customer", "rulesets_version", "article_id") DO NOTHING')
              .values([reportArticleContextualCategory]).execute();
          });

          await Promise.all(insertRatingsPromise);
        } catch (e) {
          this.logger.error(e, e.query);
        }
      })
    );
  }

  public async transformRuleset(rulesetId: number) {
    this.incrementActiveTransformations();
    const mantisConnection = await this.mantis.getConnection();
    const reportConnection = await this.report.getConnection();

    try {
      const mantisRuleset = await mantisConnection
        .getRepository(Mantis.Ruleset)
        .find({ where: { id: rulesetId }, relations: ['segments']})

      if (typeof mantisRuleset === "undefined" || mantisRuleset.length === 0) {
        this.logger.error(
          `Failed at fetching ruleset ID ${rulesetId} -- skipping.`
        );
        this.decrementActiveTransformations();
        return;
      }

      const reportRuleset: Report.Ruleset = reportConnection
        .getRepository(Report.Ruleset)
        .merge(new Report.Ruleset(), mantisRuleset[0]);

      await this.saveReportRuleset(reportConnection, reportRuleset);

    } catch (e) {
      this.logger.error(e, e.query);
    }

    this.decrementActiveTransformations();
  }

  public async saveReportRuleset(reportConnection: Connection, ruleset: Report.Ruleset) {
    // this is a nightmare
    // adding cascade on insert/update was causing contextual segments to error if they were already inserted.
    // i cant do multi value upserts with this thing either, the only way it seemed was to do it one by one.
    const segments: Promise<InsertResult>[] = ruleset.segments.map(segment => reportConnection.createQueryBuilder().insert().into(Report.ContextualSegment).onConflict('("id") UPDATE').values(segment).execute());

    
    let result = null;
    try {
      result = await Promise.all(segments);
    } catch (error) {
      throw error;
    }
  
    try {
      await reportConnection.manager.save(ruleset);
    } catch(error) {
      throw error;
    }

    try {
      await reportConnection.createQueryBuilder()
      .insert()
      .into("rulesets_contextual_segments")
      .onConflict('("rulesets_id", "contextual_segments_id") DO NOTHING')
      .values(ruleset.segments.map((segment) => ({ 'rulesets_id': ruleset.id , 'contextual_segments_id': segment.id })))
      .execute();
    } catch(error) {
      throw error;
    }
  }

  private async transformCategories(
    articleId: number,
    reportArticle: Report.Article,
    categories: Category[]
  ) {
    if (typeof categories !== "undefined") {
      await Promise.all(
        categories.map(
          this.report.saveCategory(articleId, reportArticle).bind(this.report)
        )
      );
    }
  }

  private async transformKeywords(
    articleId: number,
    reportArticle: Report.Article,
    keywords: Keyword[]
  ) {
    if (typeof keywords !== "undefined") {
      await Promise.all(
        keywords.map(
          this.report.saveKeyword(articleId, reportArticle).bind(this.report)
        )
      );
    }
  }

  private async transformEntities(
    articleId: number,
    reportArticle: Report.Article,
    entities: TextEntity[]
  ) {
    if (typeof entities !== "undefined") {
      await Promise.all(
        entities.map(
          this.report.saveEntity(articleId, reportArticle).bind(this.report)
        )
      );
    }
  }

  private async transformConcepts(
    articleId: number,
    reportArticle: Report.Article,
    concepts: Concept[]
  ) {
    if (typeof concepts !== "undefined") {
      await Promise.all(
        concepts.map(
          this.report.saveConcept(articleId, reportArticle).bind(this.report)
        )
      );
    }
  }

  public async transformArticle(articleId: number) {
    this.incrementActiveTransformations();
    const transformationStart: number = Date.now();
    const reportConnection = await this.report.getConnection();
    const mantisArticle = await this.mantis.getArticleById(articleId);

    if (typeof mantisArticle === "undefined") {
      this.logger.error(
        `Failed at fetching article ID ${articleId} -- skipping.`
      );
      this.decrementActiveTransformations();
      return;
    }

    const analysisResult = mantisArticle.findings?.findings?.text
    const analysisInput = mantisArticle.findings.input

    const reportArticle: Report.Article = reportConnection
      .getRepository(Report.Article)
      .merge(new Report.Article(), mantisArticle, {
        publication_date: analysisInput?.published || null,
        sentiment_label: analysisResult?.sentiment?.document?.label || null,
        sentiment_score: analysisResult?.sentiment?.document?.score || null,
        emotion_anger: analysisResult?.emotion?.document?.emotion?.anger || null,
        emotion_disgust: analysisResult?.emotion?.document?.emotion?.disgust || null,
        emotion_fear: analysisResult?.emotion?.document?.emotion?.fear || null,
        emotion_joy: analysisResult?.emotion?.document?.emotion?.joy || null,
        emotion_sadness: analysisResult?.emotion?.document?.emotion?.sadness || null,
      });

    try {
      await reportConnection
        .getRepository(Report.Article)
        .insert(reportArticle);
    } catch (e) {
      this.logger.error(
        `Failed at writing article ID ${articleId} -- skipping. ${e}\n${e.query}\n${JSON.stringify(reportArticle, undefined, 2)}`
      );
      this.decrementActiveTransformations();
      return;
    }

    if (analysisResult) {
      await Promise.all([
        this.transformCategories(articleId, reportArticle, analysisResult.categories),
        this.transformKeywords(articleId, reportArticle, analysisResult.keywords),
        this.transformEntities(articleId, reportArticle, analysisResult.entities),
        this.transformConcepts(articleId, reportArticle, analysisResult.concepts),
      ]);
    }

    await this.transformRatings(articleId, reportArticle);
    await this.transformMantisArticleCategories(articleId, reportArticle);

    this.logger.info(`Processed articles: ${++this.processedArticles}`);
    this.logger.time("Article transformation", transformationStart);

    this.decrementActiveTransformations();
  }
}
