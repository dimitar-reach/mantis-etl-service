import { Inject, Provides, Singleton } from 'typescript-ioc'
import Timeout = NodeJS.Timeout

import * as Mantis from '../entities/mantis'
import * as Report from '../entities/report'
import { SyncApi } from './sync.api'
import { LoggerApi } from '../logger'
import { ReportApi } from './report.api'
import { MantisApi } from './mantis.api'
import { TransformApi } from './transform.api'
import { sleep } from '../util/timer'

@Singleton
@Provides(SyncApi)
export class SyncService implements SyncApi {
  @Inject mantis: MantisApi
  @Inject report: ReportApi
  @Inject transform: TransformApi
  logger: LoggerApi
  running: boolean
  timer: Timeout

  constructor(
    @Inject logger: LoggerApi
  ) {
    this.logger = logger.child('SyncService');
    this.running = false
  }

  private setRunningFalse() {
    this.running = false
  }

  private setRunningTrue() {
    this.running = true
  }


  public start() {
    this._startSync()
    this.timer = setInterval(() => this._startSync(), 1000 * 60 * 5) // Sync every 5 minutes
  }


  private _startSync() {
    if (!this.running) {
      this.sync()
    }
  }


  private async sync() {
    this.setRunningTrue()

    try {
      await this.syncRulesets()
      await this.syncArticles()

    } catch (e) {
      this.logger.error('Encountered error during sync process. Terminating running sync.\n', e)

    } finally {
      this.setRunningFalse()
    }
  }

  private async syncArticles() {
    const areArticlesSynced = await this.getArticleSyncStatus()

    if (!areArticlesSynced) {
      const syncedReportArticles: number[] = await this.getAllReportArticleIds()

      if (typeof syncedReportArticles === "undefined") {
        this.logger.error('Failed to fetch articles from report database. Aborting.')
        return
      }

      const unsyncedMantisArticles: number[] = await this.getUnsyncedMantisArticleIds(syncedReportArticles)

      if (typeof unsyncedMantisArticles === "undefined") {
        this.logger.error('Failed to fetch articles from mantis database. Aborting.')
        return
      }

      this.logger.info(`Found a total of ${unsyncedMantisArticles.length.toString()} new articles to transform.`)

      await unsyncedMantisArticles.reduce(async (previousPromise, nextId) => {
        await previousPromise
        return this.queueArticle(nextId)
      }, Promise.resolve())

    } else {
      this.logger.info('No new articles to transform.')
    }

  }


  private async syncRulesets() {
    const areRulesetsSynced = await this.getRulesetSyncStatus()

    if (!areRulesetsSynced) {
      const syncedReportRulesets: number[] = await this.getAllReportRulesetIds()

      if (typeof syncedReportRulesets === "undefined") {
        this.logger.error('Failed to fetch rulesets from report database. Aborting.')
        return
      }

      const unsyncedMantisRulesets: number[] = await this.getUnsyncedMantisRulesetIds(syncedReportRulesets)

      if (typeof unsyncedMantisRulesets === "undefined") {
        this.logger.error('Failed to fetch rulesets from mantis database. Aborting.')
        return
      }

      this.logger.info(`Found a total of ${unsyncedMantisRulesets.length.toString()} new rulesets to transform.`)

      await unsyncedMantisRulesets.reduce(async (previousPromise, nextId) => {
        await previousPromise
        return this.queueRuleset(nextId)
      }, Promise.resolve())

    } else {
      this.logger.info('No new rulesets to transform.')
    }
  }


  private async getArticleSyncStatus(): Promise<boolean> {
    const reportConnection = await this.report.getConnection()
    const mantisConnection = await this.mantis.getConnection()

    const mantisArticleCount: number = await mantisConnection
      .getRepository(Mantis.Article)
      .createQueryBuilder()
      .getCount()

    const reportArticleCount: number = await reportConnection
      .getRepository(Report.Article)
      .createQueryBuilder()
      .getCount()

    return mantisArticleCount === reportArticleCount;
  }


  private async getRulesetSyncStatus(): Promise<boolean> {
    const reportConnection = await this.report.getConnection()
    const mantisConnection = await this.mantis.getConnection()

    const mantisRulesetCount: number = await mantisConnection
      .getRepository(Mantis.Ruleset)
      .createQueryBuilder()
      .getCount();
    
    const mantisContextualSegmentCount =  await mantisConnection.getRepository(Mantis.ContextualSegment).createQueryBuilder().getCount();

    const reportRulesetCount: number = await reportConnection
      .getRepository(Report.Ruleset)
      .createQueryBuilder()
      .getCount();

    const reportContextualSegmentCount =  await reportConnection.getRepository(Report.ContextualSegment).createQueryBuilder().getCount();

    return mantisRulesetCount === reportRulesetCount && mantisContextualSegmentCount === reportContextualSegmentCount;
  }


  private async getAllReportArticleIds(): Promise<number[]> {
    const reportConnection = await this.report.getConnection()

    try {
      const reportArticles: Report.Article[] = await reportConnection
        .getRepository(Report.Article)
        .createQueryBuilder('article')
        .select('article.id')
        .orderBy('article.id', 'ASC')
        .getMany()

      return reportArticles.map(article => article.id)

    } catch (e) {
      this.logger.error(e, e.query)
    }
  }


  private async getAllReportRulesetIds(): Promise<number[]> {
    const reportConnection = await this.report.getConnection()

    try {
      const reportRulesets: Report.Ruleset[] = await reportConnection
        .getRepository(Report.Ruleset)
        .createQueryBuilder('ruleset')
        .select('ruleset.id')
        .orderBy('ruleset.id', 'ASC')
        .getMany()

      return reportRulesets.map(ruleset => ruleset.id)

    } catch (e) {
      this.logger.error(e, e.query)
    }
  }


  private async getUnsyncedMantisArticleIds(reportArticles: number[]): Promise<number[]> {
    const mantisConnection = await this.mantis.getConnection()
    const startTime: number = Date.now()

    try {
      const allMantisArticles: Mantis.Article[] = await mantisConnection
        .getRepository(Mantis.Article)
        .createQueryBuilder('article')
        .select('article.id')
        .orderBy('article.id', 'ASC')
        .getMany()


      const allMantisArticleIds: Set<number> = new Set(allMantisArticles.map(article => article.id))
      const allReportArticleIds: Set<number> = new Set(reportArticles)

      const unsyncedArticles = new Set([...allMantisArticleIds]
        .filter(rulesetId => !allReportArticleIds.has(rulesetId)));

      this.logger.time('Checking for new articles', startTime)
      return Array.from(unsyncedArticles)

    } catch (e) {
      this.logger.error(e, e.query)
    }
  }


  private async getUnsyncedMantisRulesetIds(reportRulesets: number[]): Promise<number[]> {
    const mantisConnection = await this.mantis.getConnection()
    const startTime: number = Date.now()

    try {
      const allMantisRulesets: Mantis.Ruleset[] = await mantisConnection
        .getRepository(Mantis.Ruleset)
        .createQueryBuilder('ruleset')
        .select('ruleset.id')
        .orderBy('ruleset.id', 'ASC')
        .getMany()

      const allMantisRulesetIds: Set<number> = new Set(allMantisRulesets.map(ruleset => ruleset.id))
      const allReportRulesetIds: Set<number> = new Set(reportRulesets)

      const unsyncedRulesets = new Set([...allMantisRulesetIds]
        .filter(rulesetId => !allReportRulesetIds.has(rulesetId)));

      this.logger.time('Checking for new rulesets', startTime)
      return Array.from(unsyncedRulesets)

    } catch (e) {
      this.logger.error(e, e.query)
    }
  }


  private async queueArticle(articleId: number) {
    if (this.transform.activeTransformations <= 10) {
      this.transform.transformArticle(articleId)
      this.logger.info(`Pushed article ID ${articleId} into queue.`)
    } else {
      await sleep(500)
      await this.queueArticle(articleId)
    }
  }


  private async queueRuleset(rulesetId: number) {
    if (this.transform.activeTransformations <= 10) {
      this.transform.transformRuleset(rulesetId)
      this.logger.info(`Pushed ruleset ID ${rulesetId} into queue.`)
    } else {
      await sleep(500)
      await this.queueRuleset(rulesetId)
    }
  }


}
