export abstract class TransformApi {

  activeTransformations: number

  abstract async transformArticle(articleId: number): Promise<void>

  abstract async transformRuleset(rulesetId: number): Promise<void>


}
