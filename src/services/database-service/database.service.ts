import { DatabaseApi } from './database.api'
import { Inject, Provides, Singleton } from 'typescript-ioc'
import { LoggerApi } from '../../logger'
import { Connection, ConnectionOptions, createConnection } from 'typeorm'

@Singleton
@Provides(DatabaseApi)
export class DatabaseService implements DatabaseApi {
  @Inject logger: LoggerApi
  private readonly config: ConnectionOptions
  private _connection: Connection

  constructor(
    serviceName: string,
    config: ConnectionOptions
  ) {
    this.logger = this.logger.child(`${serviceName}DatabaseService`);
    this.config = config
  }

  async getConnection(): Promise<Connection> {
    if (!this._connection?.isConnected) {
      this.logger.info('Detected closed database connection. Reconnecting..')
      await this.connect()
    }
    return this._connection
  }

  async connect(): Promise<any> {
    try {
      this.logger.info(`Connecting to database...`)
      this._connection = await createConnection(this.config)
      this.logger.info(`Connection to database successful!`)
    } catch (err) {
      this.logger.error(`Database connection failed.`)
      throw new Error(err)
    }
  }


}
