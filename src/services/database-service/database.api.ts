import { Connection } from 'typeorm'

export abstract class DatabaseApi {
  abstract async connect(): Promise<void>

  abstract async getConnection(): Promise<Connection>

}
