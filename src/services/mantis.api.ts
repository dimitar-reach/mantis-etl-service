import { Findings } from '../typings'
import { DatabaseApi } from './database-service'
import { Article, Rating, User, Category } from '../entities/mantis'

export abstract class MantisApi extends DatabaseApi {

  abstract async getArticleFindings(id: number): Promise<Findings>

  abstract async getArticleById(id: number): Promise<Article>

  abstract async getRatingsByArticleId(articleId: number): Promise<Rating[]>

  abstract async getAllRulesetIDs(): Promise<number[]>

  abstract async getAllArticleIDs(): Promise<number[]>

  abstract async getUserNameById(id: string): Promise<string>;

  abstract async getCategoriesByArticleId(articleId: number): Promise<Category[]>;

}
