import { Inject, Provides, Singleton } from "typescript-ioc";

import { DatabaseService } from "./database-service";
import { ServiceConfig } from "../config/service.config";
import { ReportApi } from "./report.api";
import { Category, Concept, Keyword, TextEntity } from "../typings";
import * as Report from "../entities/report";
import { QueryFailedError } from "typeorm";
import { parseCategory } from "../util/parse-category";

@Singleton
@Provides(ReportApi)
export class ReportService extends DatabaseService implements ReportApi {
  constructor(@Inject config: ServiceConfig) {
    super("Report", config.report);
  }

  private handleQueryError(e) {
    this.logger.error(e, e.query);
  }

  // public async getCategoryByLabel(label: string): Promise<Report.Category> {
  //   const reportConnection = await this.getConnection()
  //
  //   const labelsArr: string[][] = parseCategory(label)
  //   const labelsObj = Object.fromEntries(labelsArr)
  //
  //   const [where_1, where_2, where_3, where_4, where_5] = [...Array(5).keys()]
  //     .map((_, i) => {
  //       const levelIndex = i + 1
  //       const levelProp = labelsObj['level_' + levelIndex]
  //       return `category.level_${levelIndex} ${levelProp ? `= '${levelProp}'` : 'is null'}`
  //     })
  //
  //   try {
  //     const category = await reportConnection
  //       .getRepository(Report.Category)
  //       .createQueryBuilder('category')
  //       .where(where_1)
  //       .andWhere(where_2)
  //       .andWhere(where_3)
  //       .andWhere(where_4)
  //       .andWhere(where_5)
  //       .getOne()
  //
  //     return category
  //
  //   } catch (e) {
  //     this.logger.error(e, e.query)
  //   }
  // }

  public saveEntity(article_id: number, reportArticle: Report.Article) {
    return async function (
      this,
      { type, text, disambiguation, count, relevance }: TextEntity
    ) {
      const reportConnection = await this.getConnection();

      const dbpedia_resource = disambiguation?.dbpedia_resource;

      try {
        // const reportEntity = reportConnection
        //   .getRepository(Report.EntityClass)
        //   .create({
        //     type,
        //     text,
        //     dbpedia_resource,
        //     count,
        //     relevance,
        //     article_id,
        //   });
        //
        // await reportConnection
        //   .getRepository(Report.EntityClass)
        //   .insert(reportEntity);

        const reportArticleEntities = reportConnection
          .getRepository(Report.ArticleEntities)
          .create({
            ...reportArticle,
            type,
            entity: text,
            dbpedia_resource,
            count,
            relevance,
            article_id,
          });

        await reportConnection
          .getRepository(Report.ArticleEntities)
          .insert(reportArticleEntities);
      } catch (e) {
        if (e instanceof QueryFailedError) {
          this.handleQueryError(e);
        }
      }
    };
  }

  public saveCategory(article_id: number, reportArticle: Report.Article) {
    return async function (this, { label, score }: Category) {
      const reportConnection = await this.getConnection();

      const labelsArr: string[][] = parseCategory(label);
      const labelsObj = Object.fromEntries(labelsArr);

      try {
        // const reportCategory = reportConnection
        //   .getRepository(Report.Category)
        //   .create({ ...labelsObj, score, article_id });
        //
        // await reportConnection
        //   .getRepository(Report.Category)
        //   .insert(reportCategory);

        const reportArticleCategories = reportConnection
          .getRepository(Report.ArticleCategories)
          .create({ ...reportArticle, ...labelsObj, score, article_id });

        await reportConnection
          .getRepository(Report.ArticleCategories)
          .insert(reportArticleCategories);
      } catch (e) {
        if (e instanceof QueryFailedError) {
          this.handleQueryError(e);
        }
      }
    };
  }

  public saveKeyword(article_id: number, reportArticle: Report.Article) {
    return async function (this, { text, relevance, count }: Keyword) {
      const reportConnection = await this.getConnection();

      try {
        // const reportKeyword = reportConnection
        //   .getRepository(Report.Keyword)
        //   .create({
        //     text,
        //     relevance,
        //     count,
        //     article_id,
        //   });
        //
        // await reportConnection
        //   .getRepository(Report.Keyword)
        //   .insert(reportKeyword);

        const reportArticleKeywords = reportConnection
          .getRepository(Report.ArticleKeywords)
          .create({
            ...reportArticle,
            keyword: text,
            relevance,
            count,
            article_id,
          });

        await reportConnection
          .getRepository(Report.ArticleKeywords)
          .insert(reportArticleKeywords);
      } catch (e) {
        if (e instanceof QueryFailedError) {
          this.handleQueryError(e);
        }
      }
    };
  }

  public saveConcept(article_id: number, reportArticle: Report.Article) {
    return async function (
      this,
      { text, dbpedia_resource, relevance }: Concept
    ) {
      const reportConnection = await this.getConnection();

      try {
        // const reportConcept = reportConnection
        //   .getRepository(Report.Concept)
        //   .create({
        //     text,
        //     dbpedia_resource,
        //     relevance,
        //     article_id,
        //   });
        //
        // await reportConnection
        //   .getRepository(Report.Concept)
        //   .insert(reportConcept);

        const reportArticleConcepts = reportConnection
          .getRepository(Report.ArticleConcepts)
          .create({
            ...reportArticle,
            concept: text,
            dbpedia_resource,
            relevance,
            article_id,
          });

        await reportConnection
          .getRepository(Report.ArticleConcepts)
          .insert(reportArticleConcepts);

      } catch (e) {
        if (e instanceof QueryFailedError) {
          this.handleQueryError(e);
        }
      }
    };
  }
}
