import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions'
import { BasicAuthMiddlewareOptions } from 'express-basic-auth'

export type MantisServiceConfig =  PostgresConnectionOptions

export type ReportServiceConfig =  PostgresConnectionOptions

export type ServerConfig = {
  auth: BasicAuthMiddlewareOptions
}
