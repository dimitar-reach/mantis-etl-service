export interface Findings {
    input?:    Input;
    ratings?:  RatingElement[];
    findings?: FindingsClass;
}

export interface FindingsClass {
    text?:   FindingsText;
    images?: string[];
}

export interface FindingsText {
    usage?:          Usage;
    syntax?:         Syntax;
    sentiment?:      Sentiment;
    semantic_roles?: SemanticRole[];
    relations?:      Relation[];
    language?:       string;
    keywords?:       Keyword[];
    entities?:       TextEntity[];
    emotion?:        TextEmotion;
    concepts?:       Concept[];
    categories?:     Category[];
    warnings?:       string[];
    analyzed_text?:  string;
    retrieved_url?:  string;
    metadata?:       Metadata;
}

export interface Category {
    score?:       number;
    label?:       string;
    explanation?: Explanation;
}

export interface Explanation {
    relevant_text?: RelevantText[];
}

export interface RelevantText {
    text?: string;
}

export interface Concept {
    text?:             string;
    relevance?:        number;
    dbpedia_resource?: string;
}

export interface TextEmotion {
    document?: EmotionDocument;
}

export interface EmotionDocument {
    emotion?: DocumentEmotion;
}

export interface DocumentEmotion {
    sadness?: number;
    joy?:     number;
    fear?:    number;
    disgust?: number;
    anger?:   number;
}

export interface TextEntity {
    type?:           string;
    text?:           string;
    relevance?:      number;
    mentions?:       Sentence[];
    count?:          number;
    disambiguation?: TextEntityDisambiguation;
}

export interface TextEntityDisambiguation {
    subtype?:          string[];
    name?:             string;
    dbpedia_resource?: string;
}

export interface Sentence {
    text?:     string;
    location?: number[];
}

export interface Keyword {
    text?:      string;
    relevance?: number;
    count?:     number;
}

export interface Metadata {
    title?:            string;
    publication_date?: string;
    image?:            string;
    feeds?:            string[];
    authors?:          Author[];
}

export interface Author {
    name?: string;
}

export interface Relation {
    type?:      string;
    sentence?:  string;
    score?:     number;
    arguments?: Argument[];
}

export interface Argument {
    text?:     string;
    location?: number[];
    entities?: ArgumentEntity[];
}

export interface ArgumentEntity {
    type?:           string;
    text?:           string;
    disambiguation?: ArgumentEntityDisambiguation;
}

export interface ArgumentEntityDisambiguation {
    subtype?: string[];
}

export interface SemanticRole {
    subject?:  RelevantText;
    sentence?: string;
    object?:   RelevantText;
    action?:   Action;
}

export interface Action {
    verb?:       Verb;
    text?:       string;
    normalized?: string;
}

export interface Verb {
    text?:    string;
    tense?:   Tense;
    negated?: boolean;
}

export enum Tense {
    Future = "future",
    Past = "past",
    Present = "present",
}

export interface Sentiment {
    document?: SentimentDocument;
}

export interface SentimentDocument {
    score?: number;
    label?: string;
}

export interface Syntax {
    tokens?:    Sentence[];
    sentences?: Sentence[];
}

export interface Usage {
    text_units?:      number;
    text_characters?: number;
    features?:        number;
}

export interface Input {
    html?:      string;
    cmsID?:     string;
    url?:       string;
    title?:     string;
    author?:    string;
    published?: string;
    images?:    string[];
}

export interface RatingElement {
    customer?:       string;
    rating?:         RatingEnum;
    text?:           RatingText;
    images?:         string[];
    ruleSetVersion?: number;
}

export enum RatingEnum {
    Green = "GREEN",
    Red = "RED",
    Amber = 'AMBER'
}

export interface RatingText {
    rating?: RatingEnum;
    rules?:  RuleElement[];
}

export interface RuleElement {
    type?:       string;
    rule?:       RuleEnum;
    rating?:     RatingEnum;
    name?:       string;
    score?:      number;
    pattern?:    string;
    threshold?:  number | '-';
    entityType?: string;
    count?:      number;
}


export enum RuleEnum {
    Category = "category",
    Concept = "concept",
    Entity = "entity",
    Keyword = "keyword",
}

