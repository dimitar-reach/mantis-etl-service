import { MantisServiceConfig, ReportServiceConfig, ServerConfig } from '../typings'
import * as ReportEntities from '../entities/report'
import * as MantisEntities from '../entities/mantis'
import { Provides, Singleton } from 'typescript-ioc'
import { getEnv } from '../util/get-env'


@Singleton
@Provides(ServiceConfig)
export class ServiceConfig {
  mantis: MantisServiceConfig
  report: ReportServiceConfig
  server: ServerConfig
  dev: boolean

  constructor() {
    this.dev = getEnv('NODE_ENV', 'development') !== 'production'
    this.mantis = this.getMantisConfig()
    this.server = this.getServerConfig()
    this.report = this.getReportConfig()
  }

  private getServerConfig(): ServerConfig {
    return {
      auth: {
        users: {
          [getEnv('AUTH_USER', 'admin')]: getEnv('AUTH_PASS', 'Reach-ETL-Dev')
        },
        challenge: true
      }
    }
  }

  private getReportConfig(): ReportServiceConfig {
    return {
      type: 'postgres',
      name: 'report',
      host: getEnv('REPORT_DB_HOST'),
      port: +getEnv('REPORT_DB_PORT'),
      username: getEnv('REPORT_DB_USER'),
      password: getEnv('REPORT_DB_PASS'),
      database: getEnv('REPORT_DB_NAME'),
      schema: getEnv('REPORT_DB_SCHEMA', 'public'),
      dropSchema: false,
      synchronize: this.dev,
      logging: false,
      entities: [...Object.values(ReportEntities)],
      ssl: {
        rejectUnauthorized: false
      },
    }
  }

  private getMantisConfig(): MantisServiceConfig {
    return {
      type: 'postgres',
      name: 'mantis',
      host: getEnv('MANTIS_DB_HOST'),
      port: +getEnv('MANTIS_DB_PORT'),
      username: getEnv('MANTIS_DB_USER'),
      password: getEnv('MANTIS_DB_PASS'),
      database: getEnv('MANTIS_DB_NAME'),
      schema: getEnv('MANTIS_DB_SCHEMA', 'public'),
      synchronize: false,
      logging: false,
      entities: [...Object.values(MantisEntities)],
      ssl: {
        rejectUnauthorized: false
      },
    }
  }
}
