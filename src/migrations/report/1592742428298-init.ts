import {MigrationInterface, QueryRunner} from "typeorm";

export class init1592742428298 implements MigrationInterface {
    name = 'init1592742428298'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "public"."rulesets" ("id" integer NOT NULL, "version" integer NOT NULL, "creation_date" TIMESTAMP, "user_id" character varying, "ruleset" text, CONSTRAINT "UQ_9a6e5f7bd9173e630d800f66d11" UNIQUE ("version"), CONSTRAINT "PK_47a4b1e70f5c43ecab715d3703c" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "public"."articles" ("id" integer NOT NULL, "creation_date" TIMESTAMP NOT NULL, "cms_id" character varying, "url" character varying, "text" character varying, "publication_date" date, "sentiment_label" character varying, "sentiment_score" real, "emotion_sadness" real, "emotion_anger" real, "emotion_disgust" real, "emotion_fear" real, "emotion_joy" real, CONSTRAINT "PK_dc762154740b66a705f33e9016c" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "public"."article_categories" ("unique_key" SERIAL NOT NULL, "article_id" integer NOT NULL, "creation_date" TIMESTAMP NOT NULL, "cms_id" character varying, "url" character varying, "text" character varying, "publication_date" date, "sentiment_label" character varying, "sentiment_score" real, "emotion_sadness" real, "emotion_anger" real, "emotion_disgust" real, "emotion_fear" real, "emotion_joy" real, "level_1" character varying, "level_2" character varying, "level_3" character varying, "level_4" character varying, "level_5" character varying, "score" real NOT NULL, CONSTRAINT "PK_c841b521e4af06341924223ecb3" PRIMARY KEY ("unique_key"))`, undefined);
        await queryRunner.query(`CREATE TABLE "public"."article_concepts" ("unique_key" SERIAL NOT NULL, "article_id" integer NOT NULL, "creation_date" TIMESTAMP NOT NULL, "cms_id" character varying, "url" character varying, "text" character varying, "publication_date" date, "sentiment_label" character varying, "sentiment_score" real, "emotion_sadness" real, "emotion_anger" real, "emotion_disgust" real, "emotion_fear" real, "emotion_joy" real, "concept" character varying NOT NULL, "dbpedia_resource" character varying, "relevance" real NOT NULL, CONSTRAINT "PK_6109467cf6625a5534ee4f89153" PRIMARY KEY ("unique_key"))`, undefined);
        await queryRunner.query(`CREATE TABLE "public"."article_entities" ("unique_key" SERIAL NOT NULL, "article_id" integer NOT NULL, "creation_date" TIMESTAMP NOT NULL, "cms_id" character varying, "url" character varying, "text" character varying, "publication_date" date, "sentiment_label" character varying, "sentiment_score" real, "emotion_sadness" real, "emotion_anger" real, "emotion_disgust" real, "emotion_fear" real, "emotion_joy" real, "dbpedia_resource" character varying, "type" character varying NOT NULL, "entity" character varying NOT NULL, "count" real NOT NULL, "relevance" real NOT NULL, CONSTRAINT "PK_8c831d46350768a815c71e380e3" PRIMARY KEY ("unique_key"))`, undefined);
        await queryRunner.query(`CREATE TABLE "public"."article_keywords" ("unique_key" SERIAL NOT NULL, "article_id" integer NOT NULL, "creation_date" TIMESTAMP NOT NULL, "cms_id" character varying, "url" character varying, "text" character varying, "publication_date" date, "sentiment_label" character varying, "sentiment_score" real, "emotion_sadness" real, "emotion_anger" real, "emotion_disgust" real, "emotion_fear" real, "emotion_joy" real, "keyword_creation_date" TIMESTAMP NOT NULL DEFAULT now(), "keyword" character varying NOT NULL, "count" real NOT NULL, "relevance" real NOT NULL, CONSTRAINT "PK_43b4f4153fcc42676773c6f268c" PRIMARY KEY ("unique_key"))`, undefined);
        await queryRunner.query(`CREATE TABLE "public"."article_ratings" ("unique_key" SERIAL NOT NULL, "article_id" integer NOT NULL, "creation_date" TIMESTAMP NOT NULL, "cms_id" character varying, "url" character varying, "text" character varying, "publication_date" date, "sentiment_label" character varying, "sentiment_score" real, "emotion_sadness" real, "emotion_anger" real, "emotion_disgust" real, "emotion_fear" real, "emotion_joy" real, "customer" character varying, "calculated_rating" character varying, "expected_rating" character varying, "review_date" TIMESTAMP, "review_user_id" character varying, "comment" character varying, "rulesets_version" integer NOT NULL, CONSTRAINT "PK_4c40a97a323c8db35f5fc8a3880" PRIMARY KEY ("unique_key"))`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "public"."article_ratings"`, undefined);
        await queryRunner.query(`DROP TABLE "public"."article_keywords"`, undefined);
        await queryRunner.query(`DROP TABLE "public"."article_entities"`, undefined);
        await queryRunner.query(`DROP TABLE "public"."article_concepts"`, undefined);
        await queryRunner.query(`DROP TABLE "public"."article_categories"`, undefined);
        await queryRunner.query(`DROP TABLE "public"."articles"`, undefined);
        await queryRunner.query(`DROP TABLE "public"."rulesets"`, undefined);
    }

}
