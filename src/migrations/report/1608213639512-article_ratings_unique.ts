import {MigrationInterface, QueryRunner} from "typeorm";

export class articleRatingsUnique1608213639512 implements MigrationInterface {
    name = 'articleRatingsUnique1608213639512'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."article_categories" DROP CONSTRAINT "FK_55194e80347f41461e6cf1d40c6"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_concepts" DROP CONSTRAINT "FK_a55731455599c1913cd0bccd061"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_entities" DROP CONSTRAINT "FK_596b43a9d40e9f8e61f5c76d124"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_keywords" DROP CONSTRAINT "FK_38ab97ec829eaeb85dfcc13235f"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_ratings" DROP CONSTRAINT "FK_cf09e2029320471da5791fd6777"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."rulesets_contextual_segments" DROP CONSTRAINT "FK_6837e373191e0b19c6a94a98588"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."rulesets_contextual_segments" DROP CONSTRAINT "FK_709ac4e59c84cb0545c4500ccb4"`, undefined);
        await queryRunner.query(`DROP INDEX IF EXISTS "public"."articles_url"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_ratings" ADD "contextual_segments_id" integer`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_ratings" ADD CONSTRAINT "UQ_8bfcd10eb70fa47168700389406" UNIQUE ("customer", "rulesets_version", "article_id")`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_categories" ADD CONSTRAINT "FK_55194e80347f41461e6cf1d40c6" FOREIGN KEY ("article_id") REFERENCES "public"."articles"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_concepts" ADD CONSTRAINT "FK_a55731455599c1913cd0bccd061" FOREIGN KEY ("article_id") REFERENCES "public"."articles"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_entities" ADD CONSTRAINT "FK_596b43a9d40e9f8e61f5c76d124" FOREIGN KEY ("article_id") REFERENCES "public"."articles"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_keywords" ADD CONSTRAINT "FK_38ab97ec829eaeb85dfcc13235f" FOREIGN KEY ("article_id") REFERENCES "public"."articles"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_ratings" ADD CONSTRAINT "FK_cf09e2029320471da5791fd6777" FOREIGN KEY ("article_id") REFERENCES "public"."articles"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."rulesets_contextual_segments" ADD CONSTRAINT "FK_6837e373191e0b19c6a94a98588" FOREIGN KEY ("rulesets_id") REFERENCES "public"."rulesets"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."rulesets_contextual_segments" ADD CONSTRAINT "FK_709ac4e59c84cb0545c4500ccb4" FOREIGN KEY ("contextual_segments_id") REFERENCES "public"."contextual_segments"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."rulesets_contextual_segments" DROP CONSTRAINT "FK_709ac4e59c84cb0545c4500ccb4"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."rulesets_contextual_segments" DROP CONSTRAINT "FK_6837e373191e0b19c6a94a98588"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_ratings" DROP CONSTRAINT "FK_cf09e2029320471da5791fd6777"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_keywords" DROP CONSTRAINT "FK_38ab97ec829eaeb85dfcc13235f"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_entities" DROP CONSTRAINT "FK_596b43a9d40e9f8e61f5c76d124"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_concepts" DROP CONSTRAINT "FK_a55731455599c1913cd0bccd061"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_categories" DROP CONSTRAINT "FK_55194e80347f41461e6cf1d40c6"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_ratings" DROP CONSTRAINT "UQ_8bfcd10eb70fa47168700389406"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_ratings" DROP COLUMN "contextual_segments_id"`, undefined);
        await queryRunner.query(`CREATE INDEX "articles_url" ON "public"."articles" ("url") `, undefined);
        await queryRunner.query(`ALTER TABLE "public"."rulesets_contextual_segments" ADD CONSTRAINT "FK_709ac4e59c84cb0545c4500ccb4" FOREIGN KEY ("contextual_segments_id") REFERENCES "public"."contextual_segments"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."rulesets_contextual_segments" ADD CONSTRAINT "FK_6837e373191e0b19c6a94a98588" FOREIGN KEY ("rulesets_id") REFERENCES "public"."rulesets"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_ratings" ADD CONSTRAINT "FK_cf09e2029320471da5791fd6777" FOREIGN KEY ("article_id") REFERENCES "public"."articles"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_keywords" ADD CONSTRAINT "FK_38ab97ec829eaeb85dfcc13235f" FOREIGN KEY ("article_id") REFERENCES "public"."articles"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_entities" ADD CONSTRAINT "FK_596b43a9d40e9f8e61f5c76d124" FOREIGN KEY ("article_id") REFERENCES "public"."articles"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_concepts" ADD CONSTRAINT "FK_a55731455599c1913cd0bccd061" FOREIGN KEY ("article_id") REFERENCES "public"."articles"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_categories" ADD CONSTRAINT "FK_55194e80347f41461e6cf1d40c6" FOREIGN KEY ("article_id") REFERENCES "public"."articles"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
    }

}
