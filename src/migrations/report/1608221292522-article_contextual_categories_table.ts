import {MigrationInterface, QueryRunner} from "typeorm";

export class articleContextualCategoriesTable1608221292522 implements MigrationInterface {
    name = 'articleContextualCategoriesTable1608221292522'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."article_categories" DROP CONSTRAINT "FK_55194e80347f41461e6cf1d40c6"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_concepts" DROP CONSTRAINT "FK_a55731455599c1913cd0bccd061"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_entities" DROP CONSTRAINT "FK_596b43a9d40e9f8e61f5c76d124"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_keywords" DROP CONSTRAINT "FK_38ab97ec829eaeb85dfcc13235f"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_ratings" DROP CONSTRAINT "FK_cf09e2029320471da5791fd6777"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."rulesets_contextual_segments" DROP CONSTRAINT "FK_6837e373191e0b19c6a94a98588"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."rulesets_contextual_segments" DROP CONSTRAINT "FK_709ac4e59c84cb0545c4500ccb4"`, undefined);
        await queryRunner.query(`CREATE TABLE "public"."article_contextual_categories" ("unique_key" SERIAL NOT NULL, "article_id" integer NOT NULL, "creation_date" TIMESTAMP NOT NULL, "cms_id" character varying, "url" character varying, "text" character varying, "publication_date" date, "sentiment_label" character varying, "sentiment_score" real, "emotion_sadness" real, "emotion_anger" real, "emotion_disgust" real, "emotion_fear" real, "emotion_joy" real, "customer" character varying, "type" text, "review_date" TIMESTAMP, "review_user_id" character varying, "comment" character varying, "rulesets_version" integer NOT NULL, "contextual_segments_id" integer, CONSTRAINT "UQ_d42bdfd8c116e0c9b6a719a9a1a" UNIQUE ("customer", "rulesets_version", "article_id"), CONSTRAINT "PK_d5a80577931cecb72d2eb3991de" PRIMARY KEY ("unique_key"))`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_categories" ADD CONSTRAINT "FK_55194e80347f41461e6cf1d40c6" FOREIGN KEY ("article_id") REFERENCES "public"."articles"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_concepts" ADD CONSTRAINT "FK_a55731455599c1913cd0bccd061" FOREIGN KEY ("article_id") REFERENCES "public"."articles"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_entities" ADD CONSTRAINT "FK_596b43a9d40e9f8e61f5c76d124" FOREIGN KEY ("article_id") REFERENCES "public"."articles"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_keywords" ADD CONSTRAINT "FK_38ab97ec829eaeb85dfcc13235f" FOREIGN KEY ("article_id") REFERENCES "public"."articles"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_ratings" ADD CONSTRAINT "FK_cf09e2029320471da5791fd6777" FOREIGN KEY ("article_id") REFERENCES "public"."articles"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_contextual_categories" ADD CONSTRAINT "FK_fa2b5ad931ec671508a93da76e7" FOREIGN KEY ("article_id") REFERENCES "public"."articles"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."rulesets_contextual_segments" ADD CONSTRAINT "FK_6837e373191e0b19c6a94a98588" FOREIGN KEY ("rulesets_id") REFERENCES "public"."rulesets"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."rulesets_contextual_segments" ADD CONSTRAINT "FK_709ac4e59c84cb0545c4500ccb4" FOREIGN KEY ("contextual_segments_id") REFERENCES "public"."contextual_segments"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."rulesets_contextual_segments" DROP CONSTRAINT "FK_709ac4e59c84cb0545c4500ccb4"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."rulesets_contextual_segments" DROP CONSTRAINT "FK_6837e373191e0b19c6a94a98588"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_contextual_categories" DROP CONSTRAINT "FK_fa2b5ad931ec671508a93da76e7"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_ratings" DROP CONSTRAINT "FK_cf09e2029320471da5791fd6777"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_keywords" DROP CONSTRAINT "FK_38ab97ec829eaeb85dfcc13235f"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_entities" DROP CONSTRAINT "FK_596b43a9d40e9f8e61f5c76d124"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_concepts" DROP CONSTRAINT "FK_a55731455599c1913cd0bccd061"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_categories" DROP CONSTRAINT "FK_55194e80347f41461e6cf1d40c6"`, undefined);
        await queryRunner.query(`DROP TABLE "public"."article_contextual_categories"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."rulesets_contextual_segments" ADD CONSTRAINT "FK_709ac4e59c84cb0545c4500ccb4" FOREIGN KEY ("contextual_segments_id") REFERENCES "public"."contextual_segments"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."rulesets_contextual_segments" ADD CONSTRAINT "FK_6837e373191e0b19c6a94a98588" FOREIGN KEY ("rulesets_id") REFERENCES "public"."rulesets"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_ratings" ADD CONSTRAINT "FK_cf09e2029320471da5791fd6777" FOREIGN KEY ("article_id") REFERENCES "public"."articles"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_keywords" ADD CONSTRAINT "FK_38ab97ec829eaeb85dfcc13235f" FOREIGN KEY ("article_id") REFERENCES "public"."articles"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_entities" ADD CONSTRAINT "FK_596b43a9d40e9f8e61f5c76d124" FOREIGN KEY ("article_id") REFERENCES "public"."articles"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_concepts" ADD CONSTRAINT "FK_a55731455599c1913cd0bccd061" FOREIGN KEY ("article_id") REFERENCES "public"."articles"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."article_categories" ADD CONSTRAINT "FK_55194e80347f41461e6cf1d40c6" FOREIGN KEY ("article_id") REFERENCES "public"."articles"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
    }

}
