import { BaseEntity, Column, PrimaryColumn } from 'typeorm'

export abstract class RulesetBase extends BaseEntity {

  @PrimaryColumn({ type: 'integer' })
  id: number

  @Column({ type: 'integer', unique: true })
  version: number

  @Column({ type: 'timestamp without time zone', nullable: true })
  creation_date: string

  @Column({ type: 'character varying', width: 256, nullable: true })
  user_id: string

  
}
