import { BaseEntity, Column, PrimaryColumn } from 'typeorm'

export abstract class ArticleBase extends BaseEntity {

  @PrimaryColumn({ type: 'integer' })
  id: number

  @Column({ type: 'timestamp without time zone' })
  creation_date: string

  @Column({ type: 'character varying', width: 2048, nullable: true })
  cms_id: string

  @Column({ type: 'character varying', width: 2048, nullable: true })
  url: string

  @Column({ type: 'character varying', width: 102400, nullable: true })
  text: string

}
