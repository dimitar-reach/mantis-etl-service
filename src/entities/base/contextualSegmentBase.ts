import { BaseEntity, Column, PrimaryColumn } from 'typeorm'

export class ContextualSegmentBase extends BaseEntity {

  @PrimaryColumn({ type: 'bigint' })
  id: number

  @Column({ type: 'character varying', width: 256, nullable: false })
  name: string

  @Column({ type: 'integer', nullable: false })
  version: string

  @Column({ type: 'timestamp without time zone' })
  creation_date: string

  @Column({ type: 'jsonb', nullable: false })
  rules: Object

  @Column({ type: 'character varying', width: 256, nullable: false })
  rules_checksum: string

  @Column('text')
  type: string
}