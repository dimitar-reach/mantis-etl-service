import { BaseEntity, Column, PrimaryColumn } from 'typeorm'

export abstract class RatingBase extends BaseEntity {

  @Column({ type: 'character varying', width: 10, nullable: true  })
  calculated_rating: string

  @Column({ type: 'character varying', width: 10, nullable: true  })
  expected_rating: string

  @Column({ type: 'timestamp without time zone', nullable: true })
  review_date: string

  @Column({ type: 'character varying', width: 256, nullable: true  })
  review_user_id: string

  @Column({ type: 'character varying', width: 2048, nullable: true  })
  comment: string
}
