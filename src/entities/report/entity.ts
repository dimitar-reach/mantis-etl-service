import { BaseEntity, Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm'
import { Article } from './'

@Entity({ name: 'entities' })
class EntityClass extends BaseEntity {

  @PrimaryGeneratedColumn({ type: 'integer' })
  id: number

  @Column({ type: 'integer' })
  article_id

  @Column({ type: 'character varying', width: 512, nullable: true })
  dbpedia_resource: string

  @Column({ type: 'character varying', width: 2048 })
  type: string

  @Column({ type: 'character varying', width: 2048 })
  text: string

  @Column({ type: 'real' })
  count: number

  @Column({ type: 'real' })
  relevance: number

  @ManyToOne(type => Article, article => article.entities, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'article_id' })
  article: Article

}
