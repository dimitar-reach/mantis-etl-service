import { Entity, Unique, Column } from 'typeorm'
import { ContextualSegmentBase } from '../base/contextualSegmentBase'

// @Unique(["version", "name"])
@Entity({ name: 'contextual_segments' })
export class ContextualSegment extends ContextualSegmentBase {}