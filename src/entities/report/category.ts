import { BaseEntity, Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm'
import { Article } from './'


@Entity({ name: 'categories' })
class Category extends BaseEntity {

  @PrimaryGeneratedColumn({ type: 'integer' })
  id: number

  @Column({ type: 'integer' })
  article_id

  @Column({ type: 'character varying', width: 2048, nullable: true })
  level_1: string

  @Column({ type: 'character varying', width: 2048, nullable: true })
  level_2: string

  @Column({ type: 'character varying', width: 2048, nullable: true })
  level_3: string

  @Column({ type: 'character varying', width: 2048, nullable: true })
  level_4: string

  @Column({ type: 'character varying', width: 2048, nullable: true })
  level_5: string

  @Column({ type: 'real' })
  score: number

  @ManyToOne(type => Article, article => article.categories, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'article_id' })
  article: Article

}
