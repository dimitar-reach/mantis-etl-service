import { BaseEntity, Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, } from 'typeorm'
import { Article } from './'

@Entity({ name: 'keywords' })
class Keyword extends BaseEntity {

  @PrimaryGeneratedColumn({ type: 'integer' })
  id: number

  @Column({ type: 'integer' })
  article_id

  @CreateDateColumn({ type: 'timestamp without time zone' })
  creation_date: Date

  @Column({ type: 'character varying', width: 2048 })
  text: string

  @Column({ type: 'real' })
  count: number

  @Column({ type: 'real' })
  relevance: number

  @ManyToOne(type => Article, article => article.keywords, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'article_id' })
  article: Article

}
