import { BaseEntity, Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm'
import { Article } from './'

@Entity({ name: 'concepts' })
class Concept extends BaseEntity {

  @PrimaryGeneratedColumn({ type: 'integer' })
  id: number

  @Column({ type: 'integer' })
  article_id

  @Column({ type: 'character varying', width: 2048 })
  text: string

  @Column({ type: 'character varying', width: 2048, nullable: true })
  dbpedia_resource: string

  @Column({ type: 'real' })
  relevance: number

  @ManyToOne(type => Article, article => article.concepts, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'article_id' })
  article: Article

}
