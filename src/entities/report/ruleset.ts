import { Entity, ManyToMany, JoinTable } from 'typeorm'
import { RulesetBase } from '../base/rulesetBase'
import { ContextualSegment } from './contextualSegment'

@Entity({ name: 'rulesets' })
export class Ruleset extends RulesetBase {
// there are several issues regarding ManyToMany in github with typeorm
// if cascade was true or cascade: ['insert', 'update'] cascade: false was still attempting to lo
  @ManyToMany(type => ContextualSegment, { eager: true, cascade: [ 'update' ]})
  @JoinTable({ 
    name: "rulesets_contextual_segments",
    joinColumns: [
      { name: 'rulesets_id' }
    ],
    inverseJoinColumns: [
      { name: 'contextual_segments_id' }
    ] 
  })
  segments: ContextualSegment []
}

