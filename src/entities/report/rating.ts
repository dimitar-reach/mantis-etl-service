import { PrimaryColumn, Entity, JoinColumn, Column, ManyToOne } from 'typeorm'
import { Article, Ruleset } from '.'
import { RatingBase } from '../base/ratingBase'

// NOTE: Not sure if this is being used..... DB seems to use Report.ArticleRatings and so does code
@Entity({ name: 'ratings' })
class Rating extends RatingBase {

  @PrimaryColumn({ type: 'integer' })
  id: number

  @ManyToOne(type => Article, article => article.ratings, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'articles_id' })
  article: Article

  @ManyToOne(type => Ruleset, ruleset => ruleset.version)
  @JoinColumn({ name: 'rulesets_version' })
  ruleset: Ruleset

  @Column({ type: 'character varying', width: 1024, nullable: true  })
  customer: string
}
