import {
  Column,
  Entity,
  OneToMany,
  CreateDateColumn,
  PrimaryGeneratedColumn, ManyToOne, JoinColumn, Unique,
} from "typeorm";
// import { Rating } from "./rating";
// import { Category } from "./category";
// import { Concept } from "./concept";
// import { EntityClass } from "./entity";
// import { Keyword } from "./keyword";
import { ArticleBase } from "../base";

@Entity({ name: "articles" })
export class Article extends ArticleBase {
  @Column({ type: "date", nullable: true })
  publication_date: Date;

  @Column({ type: "character varying", width: 128, nullable: true })
  sentiment_label: string;

  @Column({ type: "real", nullable: true })
  sentiment_score: number;

  @Column({ type: "real", nullable: true })
  emotion_sadness: number;

  @Column({ type: "real", nullable: true })
  emotion_anger: number;

  @Column({ type: "real", nullable: true })
  emotion_disgust: number;

  @Column({ type: "real", nullable: true })
  emotion_fear: number;

  @Column({ type: "real", nullable: true })
  emotion_joy: number;

  @OneToMany((type) => ArticleRatings, (rating) => rating.article)
  ratings: ArticleRatings[];

  @OneToMany((type) => ArticleCategories, (category) => category.article)
  categories: ArticleCategories[];

  @OneToMany((type) => ArticleConcepts, (concept) => concept.article)
  concepts: ArticleConcepts[];

  @OneToMany((type) => ArticleEntities, (entity) => entity.article)
  entities: ArticleEntities[];

  @OneToMany((type) => ArticleKeywords, (keyword) => keyword.article)
  keywords: ArticleKeywords[];
}

@Entity({ name: "article_categories" })
export class ArticleCategories {
  @PrimaryGeneratedColumn("increment", { type: "int" })
  unique_key: number;

  @Column({ type: "integer" })
  article_id: number;

  @Column({ type: "timestamp without time zone" })
  creation_date: string;

  @Column({ type: "character varying", width: 2048, nullable: true })
  cms_id: string;

  @Column({ type: "character varying", width: 2048, nullable: true })
  url: string;

  @Column({ type: "character varying", width: 102400, nullable: true })
  text: string;

  @Column({ type: "date", nullable: true })
  publication_date: Date;

  @Column({ type: "character varying", width: 128, nullable: true })
  sentiment_label: string;

  @Column({ type: "real", nullable: true })
  sentiment_score: number;

  @Column({ type: "real", nullable: true })
  emotion_sadness: number;

  @Column({ type: "real", nullable: true })
  emotion_anger: number;

  @Column({ type: "real", nullable: true })
  emotion_disgust: number;

  @Column({ type: "real", nullable: true })
  emotion_fear: number;

  @Column({ type: "real", nullable: true })
  emotion_joy: number;

  @Column({ type: "character varying", width: 2048, nullable: true })
  level_1: string;

  @Column({ type: "character varying", width: 2048, nullable: true })
  level_2: string;

  @Column({ type: "character varying", width: 2048, nullable: true })
  level_3: string;

  @Column({ type: "character varying", width: 2048, nullable: true })
  level_4: string;

  @Column({ type: "character varying", width: 2048, nullable: true })
  level_5: string;

  @Column({ type: "real" })
  score: number;

  @ManyToOne(type => Article, article => article.categories, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'article_id' })
  article: Article
}


@Entity({ name: "article_concepts" })
export class ArticleConcepts {
  @PrimaryGeneratedColumn("increment", { type: "int" })
  unique_key: number;

  @Column({ type: "integer" })
  article_id: number;

  @Column({ type: "timestamp without time zone" })
  creation_date: string;

  @Column({ type: "character varying", width: 2048, nullable: true })
  cms_id: string;

  @Column({ type: "character varying", width: 2048, nullable: true })
  url: string;

  @Column({ type: "character varying", width: 102400, nullable: true })
  text: string;

  @Column({ type: "date", nullable: true })
  publication_date: Date;

  @Column({ type: "character varying", width: 128, nullable: true })
  sentiment_label: string;

  @Column({ type: "real", nullable: true })
  sentiment_score: number;

  @Column({ type: "real", nullable: true })
  emotion_sadness: number;

  @Column({ type: "real", nullable: true })
  emotion_anger: number;

  @Column({ type: "real", nullable: true })
  emotion_disgust: number;

  @Column({ type: "real", nullable: true })
  emotion_fear: number;

  @Column({ type: "real", nullable: true })
  emotion_joy: number;

  @Column({ type: 'character varying', width: 2048 })
  concept: string

  @Column({ type: 'character varying', width: 2048, nullable: true })
  dbpedia_resource: string

  @Column({ type: 'real' })
  relevance: number

  @ManyToOne(type => Article, article => article.concepts, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'article_id' })
  article: Article

}


@Entity({ name: "article_entities" })
export class ArticleEntities {
  @PrimaryGeneratedColumn("increment", { type: "int" })
  unique_key: number;

  @Column({ type: "integer" })
  article_id: number;

  @Column({ type: "timestamp without time zone" })
  creation_date: string;

  @Column({ type: "character varying", width: 2048, nullable: true })
  cms_id: string;

  @Column({ type: "character varying", width: 2048, nullable: true })
  url: string;

  @Column({ type: "character varying", width: 102400, nullable: true })
  text: string;

  @Column({ type: "date", nullable: true })
  publication_date: Date;

  @Column({ type: "character varying", width: 128, nullable: true })
  sentiment_label: string;

  @Column({ type: "real", nullable: true })
  sentiment_score: number;

  @Column({ type: "real", nullable: true })
  emotion_sadness: number;

  @Column({ type: "real", nullable: true })
  emotion_anger: number;

  @Column({ type: "real", nullable: true })
  emotion_disgust: number;

  @Column({ type: "real", nullable: true })
  emotion_fear: number;

  @Column({ type: "real", nullable: true })
  emotion_joy: number;

  @Column({ type: 'character varying', width: 512, nullable: true })
  dbpedia_resource: string

  @Column({ type: 'character varying', width: 2048 })
  type: string

  @Column({ type: 'character varying', width: 2048 })
  entity: string

  @Column({ type: 'real' })
  count: number

  @Column({ type: 'real' })
  relevance: number

  @ManyToOne(type => Article, article => article.entities, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'article_id' })
  article: Article

}


@Entity({ name: "article_keywords" })
export class ArticleKeywords {
  @PrimaryGeneratedColumn("increment", { type: "int" })
  unique_key: number;

  @Column({ type: "integer" })
  article_id: number;

  @Column({ type: "timestamp without time zone" })
  creation_date: string;

  @Column({ type: "character varying", width: 2048, nullable: true })
  cms_id: string;

  @Column({ type: "character varying", width: 2048, nullable: true })
  url: string;

  @Column({ type: "character varying", width: 102400, nullable: true })
  text: string;

  @Column({ type: "date", nullable: true })
  publication_date: Date;

  @Column({ type: "character varying", width: 128, nullable: true })
  sentiment_label: string;

  @Column({ type: "real", nullable: true })
  sentiment_score: number;

  @Column({ type: "real", nullable: true })
  emotion_sadness: number;

  @Column({ type: "real", nullable: true })
  emotion_anger: number;

  @Column({ type: "real", nullable: true })
  emotion_disgust: number;

  @Column({ type: "real", nullable: true })
  emotion_fear: number;

  @Column({ type: "real", nullable: true })
  emotion_joy: number;

  @CreateDateColumn({ type: 'timestamp without time zone' })
  keyword_creation_date: Date

  @Column({ type: 'character varying', width: 2048 })
  keyword: string

  @Column({ type: 'real' })
  count: number

  @Column({ type: 'real' })
  relevance: number

  @ManyToOne(type => Article, article => article.keywords, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'article_id' })
  article: Article
}


@Entity({ name: "article_ratings" })
@Unique(['customer', 'rulesets_version', 'article_id',])
export class ArticleRatings {
  @PrimaryGeneratedColumn("increment", { type: "int" })
  unique_key: number;

  @Column({ type: "integer" })
  article_id: number;

  @Column({ type: "timestamp without time zone" })
  creation_date: string;

  @Column({ type: "character varying", width: 2048, nullable: true })
  cms_id: string;

  @Column({ type: "character varying", width: 2048, nullable: true })
  url: string;

  @Column({ type: "character varying", width: 102400, nullable: true })
  text: string;

  @Column({ type: "date", nullable: true })
  publication_date: Date;

  @Column({ type: "character varying", width: 128, nullable: true })
  sentiment_label: string;

  @Column({ type: "real", nullable: true })
  sentiment_score: number;

  @Column({ type: "real", nullable: true })
  emotion_sadness: number;

  @Column({ type: "real", nullable: true })
  emotion_anger: number;

  @Column({ type: "real", nullable: true })
  emotion_disgust: number;

  @Column({ type: "real", nullable: true })
  emotion_fear: number;

  @Column({ type: "real", nullable: true })
  emotion_joy: number;

  @Column({ type: 'character varying', width: 1024, nullable: true })
  customer: string

  @Column({ type: 'character varying', width: 10, nullable: true })
  calculated_rating: string

  @Column({ type: 'character varying', width: 10, nullable: true })
  expected_rating: string

  @Column({ type: 'timestamp without time zone', nullable: true })
  review_date: string

  @Column({ type: 'character varying', width: 256, nullable: true })
  review_user_id: string

  @Column({ type: 'character varying', width: 2048, nullable: true })
  comment: string

  @Column({ type: "integer" })
  rulesets_version: number;

  @Column({ type: "integer", nullable: true })
  contextual_segments_id: number;

  @ManyToOne(type => Article, article => article.ratings, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'article_id' })
  article: Article
}
@Entity({ name: "article_contextual_categories" })
@Unique(['customer', 'rulesets_version', 'article_id',])
export class ArticleContextualCategories {
  @PrimaryGeneratedColumn("increment", { type: "int" })
  unique_key: number;

  @Column({ type: "integer" })
  article_id: number;

  @Column({ type: "timestamp without time zone" })
  creation_date: string;

  @Column({ type: "character varying", width: 2048, nullable: true })
  cms_id: string;

  @Column({ type: "character varying", width: 2048, nullable: true })
  url: string;

  @Column({ type: "character varying", width: 102400, nullable: true })
  text: string;

  @Column({ type: "date", nullable: true })
  publication_date: Date;

  @Column({ type: "character varying", width: 128, nullable: true })
  sentiment_label: string;

  @Column({ type: "real", nullable: true })
  sentiment_score: number;

  @Column({ type: "real", nullable: true })
  emotion_sadness: number;

  @Column({ type: "real", nullable: true })
  emotion_anger: number;

  @Column({ type: "real", nullable: true })
  emotion_disgust: number;

  @Column({ type: "real", nullable: true })
  emotion_fear: number;

  @Column({ type: "real", nullable: true })
  emotion_joy: number;

  @Column({ type: 'character varying', width: 1024, nullable: true })
  customer: string

  @Column({ type: 'text', nullable: true })
  type: string

  @Column({ type: 'timestamp without time zone', nullable: true })
  review_date: string

  @Column({ type: 'character varying', width: 256, nullable: true })
  review_user_id: string

  @Column({ type: "integer" })
  rulesets_version: number;

  @Column({ type: "integer", nullable: true })
  contextual_segments_id: number;

  @ManyToOne(type => Article, article => article.ratings, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'article_id' })
  article: Article
}
