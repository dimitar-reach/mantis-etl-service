import { Entity, Unique, Column, ManyToMany, JoinTable } from 'typeorm'
import { ContextualSegmentBase } from '../base/contextualSegmentBase'
import { Ruleset } from './ruleset'

@Unique(["version", "name"])
@Entity({ name: 'contextual_segments' })
export class ContextualSegment extends ContextualSegmentBase {


  @ManyToMany(type => Ruleset, ruleset => ruleset.segments)
  @JoinTable({ 
    name: "rulesets_contextual_segments",
    joinColumns: [
      { name: 'contextual_segments_id' }
    ],
    inverseJoinColumns: [
      { name: 'rulesets_id' }
    ] 
  })
  rulesets: Ruleset []
}