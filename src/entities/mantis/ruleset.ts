import { Entity, ManyToMany, JoinTable } from 'typeorm'
import { RulesetBase } from '../base/rulesetBase'
import { ContextualSegment } from './contextualSegment'

@Entity({ name: 'rulesets' })
export class Ruleset extends RulesetBase {

  @ManyToMany(type => ContextualSegment, segment => segment.rulesets)
  @JoinTable({ 
    name: "rulesets_contextual_segments",
    joinColumns: [
      { name: 'rulesets_id' }
    ],
    inverseJoinColumns: [
      { name: 'contextual_segments_id' }
    ] 
  })
  segments: ContextualSegment []
}
