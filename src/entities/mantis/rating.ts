import { PrimaryColumn, Column, Entity } from 'typeorm'
import { RatingBase } from '../base/ratingBase'

@Entity({ name: 'article_ratings' })
export class Rating extends RatingBase {

  // @ManyToOne(type => Article, article => article.ratings)
  // @JoinColumn({ name: 'articles_id' })
  @PrimaryColumn({ type: 'integer' })
  articles_id: number

  // @ManyToOne(type => Ruleset, ruleset => ruleset.version)
  // @JoinColumn({ name: 'rulesets_version' })
  @PrimaryColumn({ type: 'bigint' })
  contextual_segments_id: number

}
