import { Column, Entity } from 'typeorm'
import { ArticleBase } from '../base/articleBase'
import { Findings } from '../../typings'

@Entity({ name: 'articles' })
export class Article extends ArticleBase {

  @Column({ type: 'character varying', width: 102400, nullable: true })
  html: string

  @Column({ type: 'json' })
  findings: Findings

  // @OneToMany(type => Rating, rating => rating.article)
  // ratings: Rating[]

}
