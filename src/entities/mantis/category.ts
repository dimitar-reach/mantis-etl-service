import { BaseEntity, Entity, Unique, Column, PrimaryColumn } from 'typeorm'


@Entity({ name: 'article_categories' })
export class Category extends BaseEntity {

  @PrimaryColumn({ type: 'integer' })
  articles_id: number

  @PrimaryColumn({ type: 'bigint' })
  contextual_segments_id: number

  @Column({ type: 'timestamp without time zone', nullable: true })
  creation_date: string

  @Column({ type: 'character varying', width: 256, nullable: true  })
  review_user_id: string

  @Column('text')
  type: string
}