import { BaseEntity, Entity, Unique, Column, PrimaryColumn } from 'typeorm'


@Entity({ name: 'users' })
export class User extends BaseEntity {

  @PrimaryColumn({ type: 'integer' })
  id: number


  @Column({ type: 'character varying', width: 2048, nullable: true })
  username: string
}