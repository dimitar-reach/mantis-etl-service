import { Category } from '../typings'

export function parseCategory(categoryLabel: string): string[][]

export function parseCategory(categoryObject: Category): string[][]

export function parseCategory(param: string | Category): string[][] {
  let label: string

  if (typeof param === "object") {
    label = param.label
  } else {
    label = param
  }

  return label
    .slice(1)
    .replace("'", "''")
    .split('/')
    .map((levelText, i) => [`level_${++i}`, levelText])
}

