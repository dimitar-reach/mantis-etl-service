#!/bin/bash

set_cf_vars () {
  cf set-env "$CF_APP" "MANTIS_DB_HOST" "$MANTIS_DB_HOST"
  cf set-env "$CF_APP" "MANTIS_DB_NAME" "$MANTIS_DB_NAME"
  cf set-env "$CF_APP" "MANTIS_DB_PASS" "$MANTIS_DB_PASS"
  cf set-env "$CF_APP" "MANTIS_DB_PORT" "$MANTIS_DB_PORT"
  cf set-env "$CF_APP" "MANTIS_DB_SCHEMA" "$MANTIS_DB_SCHEMA"
  cf set-env "$CF_APP" "MANTIS_DB_USER" "$MANTIS_DB_USER"
  cf set-env "$CF_APP" "REPORT_DB_HOST" "$REPORT_DB_HOST"
  cf set-env "$CF_APP" "REPORT_DB_NAME" "$REPORT_DB_NAME"
  cf set-env "$CF_APP" "REPORT_DB_PASS" "$REPORT_DB_PASS"
  cf set-env "$CF_APP" "REPORT_DB_PORT" "$REPORT_DB_PORT"
  cf set-env "$CF_APP" "REPORT_DB_SCHEMA" "$REPORT_DB_SCHEMA"
  cf set-env "$CF_APP" "REPORT_DB_USER" "$REPORT_DB_USER"
  cf restage "$CF_APP"
}

# Push app
if ! cf app "$CF_APP"; then  
  cf push "$CF_APP"
  set_cf_vars
  
else
  OLD_CF_APP="${CF_APP}-OLD-$(date +"%s")"
  rollback() {
    set +e  
    if cf app "$OLD_CF_APP"; then
      cf logs "$CF_APP" --recent
      cf delete "$CF_APP" -f
      cf rename "$OLD_CF_APP" "$CF_APP"
    fi
    exit 1
  }
  set -e
  trap rollback ERR
  cf rename "$CF_APP" "$OLD_CF_APP"
  cf push "$CF_APP"
  set_cf_vars
  cf delete "$OLD_CF_APP" -f
fi
# Export app name and URL for use in later Pipeline jobs
export CF_APP_NAME="$CF_APP"
export APP_URL=http://$(cf app $CF_APP_NAME | grep -e urls: -e routes: | awk '{print $2}')
# View logs
#cf logs "${CF_APP}" --recent


