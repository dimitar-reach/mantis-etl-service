import { ConnectionOptions } from 'typeorm'
import { ServiceConfig } from './src/config/service.config'

const { report }: ServiceConfig = new ServiceConfig()

// Example create migration command syntax:
// npm run typeorm:cli -- migration:generate -n "init" -c "report"

const ORMConfig: ConnectionOptions = {
  ...report,
  logging: true,
  synchronize: false,
  migrations: ['src/migrations/report/*.ts'],
  cli: {
    migrationsDir: 'src/migrations/report'
  }
}

export = ORMConfig
