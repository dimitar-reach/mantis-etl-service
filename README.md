# Mantis ETL Microservice
The application covers all data orchestration, transformation, and management needs for the Mantis Reporting Cognos Analytics extension. The main purpose of this service is to

1. extract JSON-encoded data from `findings` column in Mantis production database,
2. transform them into relational structure,
3. and finally load them into a reporting database which serves as the data source for Cognos Analytics.

## Data flow
![data_flow_diagram](docs/images/mantis-ETL-data_flow.png)

## Technology stack
The microservice functionality is achieved utilising the following set of technologies and libraries from the JavaScript ecosystem:

- Static typing & compilation: [TypeScript](https://www.typescriptlang.org/docs/home.html)
- Object-relational mapping: [TypeORM](https://github.com/typeorm/typeorm)
- Web server: [Express.js](https://github.com/expressjs/express)
- Logger: [Pino](https://github.com/pinojs/express-pino-logger)
- REST Services: [typescript-rest](https://github.com/thiagobustamante/typescript-rest)
- Dependency injection: [typescript-ioc](https://github.com/thiagobustamante/typescript-ioc)

The project is based on IBM Garage™ open-source `template-node-typescript` [starter-kit](https://github.com/ibm-garage-cloud/template-node-typescript).

The service assumes PostgresSQL as the database engine for both the source, and target databases. A different database can be used upon small adjustments in the `service.config.ts` [file](src/config/service.config.ts) and corresponding database driver installation as per the TypeORM installation [guide](https://github.com/typeorm/typeorm#installation).

## Environment setup
The guide assumes you have Node v12 LTS installed on your computer along with corresponding version of NPM. If not, proceed to [install Node](https://nodejs.org/en/download/) before continuing.

First, clone the repository
```
$ git clone https://{your-bitbucket-username}@bitbucket.org/trinitymirror-ondemand/mantis-etl-service.git
$ cd mantis-etl-service
```
Install required dependencies
```
$ npm install
```
Set the environment variables in your `.env` file in the **project root folder**
```
# Mantis (source) database connection details
MANTIS_DB_USER = {username}
MANTIS_DB_PASS = {password}
MANTIS_DB_HOST = {host url}
MANTIS_DB_PORT = {host port}
MANTIS_DB_SCHEMA = {database schema}
MANTIS_DB_NAME = {database name}

# Report (target) database connection details
REPORT_DB_USER = {username}
REPORT_DB_PASS = {password}
REPORT_DB_HOST = {host url}
REPORT_DB_PORT = {host port}
REPORT_DB_SCHEMA = {database schema}
REPORT_DB_NAME = {database name}

# API basic auth login details
AUTH_USER = {user login} (default "admin")
AUTH_PASS = {user password} (default "Reach-ETL-Dev")
```
Build the reporting database schema using pre-defined TypeORM [migrations](https://github.com/typeorm/typeorm/blob/master/docs/migrations.md).

![data_flow_diagram](docs/images/database_schema.png)

This will send to the database all necessary SQL queries to build the structure required for the microservice to carry out the transformations. This command uses the reporting database connection configuration specified in your `.env` file.
```
$ npm run typeorm:cli -- migration:run -c "report"
```
Check that the migration ran successfully.

```
$ npm run typeorm:cli -- migration:show -c "report"
```

You should see a line similar to the one below at the end of the command output. This signifies that the migration was applied to the database.
```
> [X] init1585336734335
```
If the box is not checked or the previous command logged an error, please check the specified connection details in your `.env` file and/or consult the TypeORM [migrations guide](https://github.com/typeorm/typeorm/blob/master/docs/migrations.md) .

That's it! You can now start the service.

### Develop
To develop locally with `nodemon` using the database connection details specified in your `.env` file, simply run
```
$ npm run dev
```
The server will reload on any file change.

#### Warning!
**DO NOT USE** production report database connection details in development mode. Note that in development mode, the `report` service database connection runs with `synchronize: true` parameter, which attempts to synchronise the specified reporting database schema structure with the entities specified in the `src/entities` folder on each connection reset and you might loose data on production instance.

Setting up a [local PostgresSQL database server](https://www.postgresql.org/download/) for development of the reporting service is highly recommended.

### Build

To build the application and execute the compiled code, run
```
$ npm run start
```
You should always try to build your application before pushing it live to catch any build-time errors, but it is not a prerequisite to build the application locally before push. It will build itself automatically on the server via the `npm run start` command.

#### Note
Unless you explicitly specify `NODE_ENV=production` environment variable before `npm run start` command, the application will load the environment variables from your local `.env` file. 


### Push
To push the service to IBM Cloud Foundry, make sure that you have the latest version of [IBM Cloud CLI](https://cloud.ibm.com/docs/cli?topic=cloud-cli-getting-started) installed.

Also make sure that all the required environment variables are specified in your Cloud Foundry application instance runtime tab.

First, login into your IBM Cloud account
```
$ ibmcloud login
```
Target the designated Cloud Foundry org and space
```
$ ibmcloud target --cf
```
Push the application live
```
$ ibmcloud cf push
```

